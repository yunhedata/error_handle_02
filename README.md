
<bean class="org.springframework.web.servlet.handler.SimpleMappingExceptionResolver">  

	<property name="exceptionMappings">  
		<props>  
			<!-- 表示当抛出NumberFormatException的时候就返回名叫number的视图 -->  
		 	<prop key="NumberFormatException">number</prop>
			<prop key="NullPointerException">null</prop>  
		</props>  
	</property>  
	
	<!-- 表示当抛出异常但没有在exceptionMappings里面找到对应的异常时 返回名叫exception的视图-->  
	<property name="defaultErrorView" value="exception"/>
	
	<!-- 定义在发生异常时视图跟返回码的对应关系 -->  
	<property name="statusCodes">
		<props>  
			<!-- 表示在发生NumberFormatException时返回视图number，然后这里定义发生异常时视图number对应的HttpServletResponse的返回码是500 -->  
			<prop key="number">500</prop>
			<prop key="null">503</prop>  
		</props>  
	</property> 
	 
	<!-- 表示在发生异常时默认的HttpServletResponse的返回码是多少，默认是200 --> 
	<property name="defaultStatusCode" value="404"/> 
	
</bean>  