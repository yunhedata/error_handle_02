package cn.yunhe.springmvc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.yunhe.springmvc.exception.AuthException;

@RestController
@RequestMapping("/user")
public class UserController {

	@RequestMapping("/edit")
	public String editUser() throws AuthException {
		throw new AuthException("用户没有登陆呢");
	}

	@RequestMapping("/get")
	public String showUser() {
		return "user";
	}
	
    //此方法抛出的异常不是由GlobalExceptionHandler处理  
    //而是在catch块处理  
    @RequestMapping("login")  
    public String login4() {   
        try {  
            throw new AuthException("业务执行异常");  
        } catch (AuthException e) {  
            e.printStackTrace();  
        }  
        return "login";  
    }  
}
