package cn.yunhe.springmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import cn.yunhe.springmvc.exception.AuthException;

public class GlobalExceptionHandler2 implements HandlerExceptionResolver {

	private static Logger logger = Logger.getLogger(GlobalExceptionHandler2.class);

	/**
	 * 进行全局异常的过滤和处理
	 */
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		// handler为当前处理器适配器执行的对象
		String message = null;
		// 判断是否为系统自定义异常。
		if (ex instanceof AuthException) {
			message = ((AuthException) ex).getMessage();
		} else {
			message = "系统出错啦，稍后再试试！";
		}

		ModelAndView modelAndView = new ModelAndView();
		// 跳转到相应的处理页面
		modelAndView.addObject("errorMsg", message);
		modelAndView.setViewName("error");

		return modelAndView;
	}

}
