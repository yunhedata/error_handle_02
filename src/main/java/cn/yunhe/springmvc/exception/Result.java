package cn.yunhe.springmvc.exception;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author YUNHE
 *
 */
public class Result {
	/**
	 * 业务代码
	 */
	private int code;
	/**
	 * 业务消息
	 */
	private String message;
	/**
	 * 业务数据
	 */
	@JsonInclude(value = Include.NON_NULL)
	private List<Object> data;

	/**
	 * 默认构造函数
	 */
	public Result() {

	}

	/**
	 * 带参数构造函数
	 * 
	 * @param code
	 * @param message
	 * @param data
	 */
	public Result(int code, String message, List<Object> data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

}
