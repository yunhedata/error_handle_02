package cn.yunhe.springmvc.exception;

public class AuthException extends Exception {

	private static final long serialVersionUID = 1L;
	private String message;

	public AuthException() {

	}

	public AuthException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
